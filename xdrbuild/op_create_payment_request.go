package xdrbuild

import (
	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
)

type CreatePaymentRequest struct {
	SourceAccountID *xdr.AccountId
	SourceBalanceID xdr.BalanceId
	Destination     xdr.PaymentOpDestination
	FeeData         xdr.PaymentFeeData
	Amount          uint64
	Subject         string
	Reference       string
	Details         json.Marshaler
	AllTasks        *uint32
}

func (op *CreatePaymentRequest) XDR() (*xdr.Operation, error) {
	var allTasksXDR xdr.Uint32
	var allTasksXDRPointer *xdr.Uint32

	if op.AllTasks != nil {
		allTasksXDR = xdr.Uint32(*op.AllTasks)
		allTasksXDRPointer = &allTasksXDR
	}

	details, err := op.Details.MarshalJSON()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal details")
	}
	creatorDetails := xdr.Longstring(details)

	return &xdr.Operation{
		SourceAccount: op.SourceAccountID,
		Body: xdr.OperationBody{
			Type: xdr.OperationTypeCreatePaymentRequest,
			CreatePaymentRequestOp: &xdr.CreatePaymentRequestOp{
				Request: xdr.CreatePaymentRequest{
					PaymentOp: xdr.PaymentOp{
						SourceBalanceId: op.SourceBalanceID,
						Destination:     op.Destination,
						Amount:          xdr.Uint64(op.Amount),
						Subject:         xdr.Longstring(op.Subject),
						Reference:       xdr.Longstring(op.Reference),
						FeeData:         op.FeeData,
					},
					Ext: xdr.CreatePaymentRequestExt{
						CreatorDetails: &creatorDetails,
						V:              xdr.LedgerVersionEmptyVersion,
					},
				},
				AllTasks: allTasksXDRPointer,
				Ext:      xdr.EmptyExt{},
			},
		},
	}, nil
}
