package xdrbuild

import (
	"gitlab.com/tokend/go/xdr"
)

type ManageLimitsOp struct {
	Details xdr.ManageLimitsOpDetails
}

func (op *ManageLimitsOp) XDR() (*xdr.Operation, error) {
	return &xdr.Operation{
		Body: xdr.OperationBody{
			Type: xdr.OperationTypeManageLimits,
			ManageLimitsOp: &xdr.ManageLimitsOp{
				Details: op.Details,
				Ext:     xdr.ManageLimitsOpExt{},
			},
		},
	}, nil
}
