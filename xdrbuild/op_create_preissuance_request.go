package xdrbuild

import (
	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
)

type CreatePreIssuanceRequest struct {
	Reference string
	Asset     string
	Amount    uint64
	Details   json.Marshaler
	AllTasks  *uint32
	Signature xdr.DecoratedSignature
}

func (op *CreatePreIssuanceRequest) XDR() (*xdr.Operation, error) {
	details, err := op.Details.MarshalJSON()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal details")
	}

	return &xdr.Operation{
		Body: xdr.OperationBody{
			Type: xdr.OperationTypeCreatePreissuanceRequest,
			CreatePreIssuanceRequest: &xdr.CreatePreIssuanceRequestOp{
				Request: xdr.PreIssuanceRequest{
					Asset:          xdr.AssetCode(op.Asset),
					Amount:         xdr.Uint64(op.Amount),
					Reference:      xdr.String64(op.Reference),
					CreatorDetails: xdr.Longstring(details),
					Signature:      op.Signature,
					Ext:            xdr.PreIssuanceRequestExt{},
				},
				AllTasks: (*xdr.Uint32)(op.AllTasks),
				Ext:      xdr.CreatePreIssuanceRequestOpExt{},
			},
		},
	}, nil
}
