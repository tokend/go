package xdrbuild

import (
	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
)

type CreateAmlAlert struct {
	Reference string
	BalanceId xdr.BalanceId
	Amount    uint64
	Details   json.Marshaler
	AllTasks  *uint32
}

func (op *CreateAmlAlert) XDR() (*xdr.Operation, error) {
	var allTasksXDR xdr.Uint32
	var allTasksXDRPointer *xdr.Uint32

	if op.AllTasks != nil {
		allTasksXDR = xdr.Uint32(*op.AllTasks)
		allTasksXDRPointer = &allTasksXDR
	}

	details, err := op.Details.MarshalJSON()
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal details")
	}

	return &xdr.Operation{
		Body: xdr.OperationBody{
			Type: xdr.OperationTypeCreateAmlAlert,
			CreateAmlAlertRequestOp: &xdr.CreateAmlAlertRequestOp{
				Reference: xdr.String64(op.Reference),
				AmlAlertRequest: xdr.AmlAlertRequest{
					BalanceId:      op.BalanceId,
					Amount:         xdr.Uint64(op.Amount),
					CreatorDetails: xdr.Longstring(details),
					Ext:            xdr.AmlAlertRequestExt{},
				},
				AllTasks: allTasksXDRPointer,
				Ext:      xdr.CreateAmlAlertRequestOpExt{},
			},
		},
	}, nil
}
