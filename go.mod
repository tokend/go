module gitlab.com/tokend/go

go 1.21.6

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-ozzo/ozzo-validation v3.0.3-0.20170913164239-85dcd8368eba+incompatible
	github.com/nullstyle/go-xdr v0.0.0-20170810174627-a875e7c9fa23
	github.com/onsi/ginkgo v1.4.0
	github.com/onsi/gomega v1.2.0
	github.com/pkg/errors v0.8.0
	github.com/spf13/cast v1.3.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/distributed_lab/corer v0.0.0-20171130114150-cbfb46525895
	gitlab.com/distributed_lab/logan v3.3.2-0.20180102172758-33632ccd6312+incompatible
	gitlab.com/tokend/keypair v0.0.0-20171129153222-c148f15306ed
	golang.org/x/crypto v0.0.0-20170930174604-9419663f5a44
)

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.9.3 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	golang.org/x/net v0.0.0-20171004034648-a04bdaca5b32 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	golang.org/x/text v0.1.1-0.20171006144033-825fc78a2fd6 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.0.0-20170812160011-eb3733d160e7 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
